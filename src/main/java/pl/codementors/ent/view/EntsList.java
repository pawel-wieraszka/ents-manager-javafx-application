package pl.codementors.ent.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.util.converter.IntegerStringConverter;
import pl.codementors.ent.model.Ent;
import pl.codementors.ent.model.Species;
import pl.codementors.ent.workers.OpenWorker;
import pl.codementors.ent.workers.SaveWorker;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for displaying list of ents. Implements Initializable interface.
 *
 * @author Paweł Wieraszka
 */
public class EntsList implements Initializable {

    private static final Logger log = Logger.getLogger(EntsList.class.getCanonicalName());

    /**
     * Table for displaying list of ents.
     */
    @FXML
    private TableView entsTable;

    /**
     * Year of planting column.
     */
    @FXML
    private TableColumn<Ent, Integer> yopColumn;

    /**
     * Height column.
     */
    @FXML
    private TableColumn<Ent, Integer> heightColumn;

    /**
     * Species column.
     */
    @FXML
    private TableColumn<Ent, String> speciesColumn;

    /**
     * Type column.
     */
    @FXML
    private TableColumn<Ent, Ent.Type> typeColumn;

    /**
     * Progress bar.
     */
    @FXML
    private ProgressBar progress;

    /**
     * Observable list of ents.
     */
    private ObservableList<Ent> ents = FXCollections.observableArrayList();

    /**
     * Resource bundles with two languages.
     */
    private ResourceBundle rb;

    /**
     * Overwritten method from Initializable interface. Fills table with list of ent. All columns are editable.
     * Sets progress bar to zero.
     * @param url
     * @param resourceBundle Bundles to be load.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        entsTable.setItems(ents);
        rb = resourceBundle;

        yopColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        yopColumn.setOnEditCommit(event -> event.getRowValue().setYearOfPlanting(event.getNewValue()));

        heightColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        heightColumn.setOnEditCommit(event -> event.getRowValue().setHeight(event.getNewValue()));

        speciesColumn.setCellFactory(ChoiceBoxTableCell.forTableColumn(Species.species));
        speciesColumn.setOnEditCommit(event -> event.getRowValue().setSpecies(event.getNewValue()));

        typeColumn.setCellFactory(ChoiceBoxTableCell.forTableColumn(Ent.Type.values()));
        typeColumn.setOnEditCommit(event -> event.getRowValue().setType(event.getNewValue()));

        progress.setProgress(0);
    }

    /**
     * Reacts for pressing open button. Creates file chooser to choose a file to be loaded and calls open method.
     * @param event Press the button.
     */
    @FXML
    private void open(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(((MenuItem)event.getTarget()).getParentPopup().getScene().getWindow());
        if (file != null) {
            open(file);
        }
    }

    /**
     * Loads a file. Creates new OpenWorker and starts a new thread. Binds progress bar with loading process.
     * @param file File to be loaded.
     */
    private void open(File file) {
        OpenWorker worker = new OpenWorker(ents, file);
        progress.progressProperty().bind(worker.progressProperty());
        new Thread(worker).start();
    }

    /**
     * Reacts for pressing save button. Creates file chooser to choose a file to be saved and calls save method.
     * @param event Press the button.
     */
    @FXML
    private void save(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showSaveDialog(((MenuItem)event.getTarget()).getParentPopup().getScene().getWindow());
        if (file != null) {
            save(file);
        }
    }

    /**
     * Saves a file. Creates new SaveWorker and starts a new thread. Binds progress bar with saving process.
     * @param file File to be saved.
     */
    private void save(File file) {
        SaveWorker worker = new SaveWorker(ents, file);
        progress.progressProperty().bind(worker.progressProperty());
        new Thread(worker).start();
    }

    /**
     * Shows alert dialog with information about ents manager.
     * @param event Press the button.
     */
    @FXML
    private void about(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(rb.getString("aboutTitle"));
        alert.setHeaderText(rb.getString("aboutHeader"));
        alert.setContentText(rb.getString("aboutContent"));
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.showAndWait();
    }

    /**
     * Adds new ent to the collection.
     * @param event Press the button.
     */
    @FXML
    private void plant(ActionEvent event) {
        ents.add(new Ent());
    }

    /**
     * Removes ent from the collection and clear selection.
     * @param event Press the button.
     */
    @FXML
    private void cut(ActionEvent event) {
        if (entsTable.getSelectionModel().getSelectedIndex() >= 0) {
            ents.remove(entsTable.getSelectionModel().getSelectedIndex());
            entsTable.getSelectionModel().clearSelection();
        }
    }

    /**
     * Roots the seedling with the same type and species as chosen ent.
     * @param event Press the button.
     */
    @FXML
    private void rootTheSeedling(ActionEvent event) {
        if (entsTable.getSelectionModel().getSelectedIndex() >= 0) {
            Ent ent = new Ent();
            Object object = entsTable.getSelectionModel().getSelectedItem();
            if (object instanceof Ent) {
                ent.setHeight(10);
                ent.setYearOfPlanting(2018);
                ent.setSpecies(((Ent) object).getSpecies());
                ent.setType(((Ent) object).getType());
            }
            ents.add(ent);
        }
    }
}
