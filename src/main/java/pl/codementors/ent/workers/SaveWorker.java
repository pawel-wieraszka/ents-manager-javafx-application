package pl.codementors.ent.workers;

import javafx.concurrent.Task;
import pl.codementors.ent.model.Ent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for saving data.
 *
 * @author Paweł Wieraszka
 */
public class SaveWorker extends Task<Void> {

    private static final Logger log = Logger.getLogger(OpenWorker.class.getCanonicalName());

    /**
     * A collection of ents.
     */
    private Collection<Ent> ents;

    /**
     * File to be saved.
     */
    private File file;

    /**
     * A constructor for SaveWorker.
     * @param ents The collection to be saved.
     * @param file The file where the data will be saved.
     */
    public SaveWorker(Collection<Ent> ents, File file) {
        this.ents = ents;
        this.file = file;
    }

    /**
     * Overwritten method from Task class. Firstly it saves how many ents are in the collection.
     * Secondly it saves each object of ent with short break (0.5s) and updates progress bar after each ent.
     * @return Nothing.
     * @throws Exception Method can throw an IOException and an InterruptedException during saving.
     */
    @Override
    protected Void call() throws Exception {
        int i = 0;
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(ents.size());
            for (Ent ent : ents) {
                try {
                    oos.writeObject(ent);
                    Thread.sleep(500);
                    updateProgress(++i, ents.size());
                } catch (InterruptedException ex) {
                    log.log(Level.WARNING, ex.getMessage(), ex);
                }
            }
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return null;
    }
}
