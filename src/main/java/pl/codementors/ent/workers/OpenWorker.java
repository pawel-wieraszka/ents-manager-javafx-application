package pl.codementors.ent.workers;

import javafx.application.Platform;
import javafx.concurrent.Task;
import pl.codementors.ent.model.Ent;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for loading data.
 *
 * @author Paweł Wieraszka
 */
public class OpenWorker extends Task<Void> {

    private static final Logger log = Logger.getLogger(OpenWorker.class.getCanonicalName());

    private Collection<Ent> ents;

    private File file;

    public OpenWorker(Collection<Ent> ents, File file) {
        this.ents = ents;
        this.file = file;
    }

    @Override
    protected Void call() throws Exception {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            int size = (Integer) ois.readObject();
            for (int i = 0; i < size; i++) {
                Ent ent = (Ent) ois.readObject();
                Thread.sleep(500);
                Platform.runLater(() -> ents.add(ent));
                updateProgress(i + 1, size);
            }
        } catch (IOException | ClassNotFoundException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return null;
    }
}
