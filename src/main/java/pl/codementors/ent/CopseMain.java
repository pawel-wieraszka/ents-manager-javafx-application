package pl.codementors.ent;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

import static javafx.application.Application.launch;

/**
 * Main class of the application.
 *
 * @author Paweł Wieraszka
 */
public class CopseMain extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Starts the stage with the scene (600x600), but firstly loads fxml with javaFX view
     * and resource bundles with two languages. Also loads css file.
     * @param stage Stage to be shown.
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Ents");

        URL fxml = this.getClass().getResource("/pl/codementors/ent/ents_list.fxml");
        ResourceBundle rb = ResourceBundle.getBundle("pl.codementors.ent.messages.ents_list_msg");

        Parent root = FXMLLoader.load(fxml, rb);

        Scene scene = new Scene(root, 600, 600);
        scene.getStylesheets().add("/pl/codementors/ent/css/ents_list.css");

        stage.setScene(scene);
        stage.show();
    }
}
