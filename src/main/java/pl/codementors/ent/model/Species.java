package pl.codementors.ent.model;

/**
 * Class with all available species.
 *
 * @author Paweł Wieraszka
 */
public class Species {
    public static String[] species = {"Arborvitae", "Black Ash", "White Ash", "Bigtooth Aspen", "Quaking Aspen",
            "Basswood", "American Beech", "Black Birch", "Gray Birch", "Paper Birch", "Yellow Birch", "Butternut",
            "Black Cherry", "Pin Cherry", "American Chestnut", "Eastern Cottonwood", "Cucumber Tree", "American Elm",
            "Slippery Elm", "Balsam Fir", "Hawthorn", "Eastern Hemlock", "Bitternut Hickory", "Pignut Hickory",
            "Shagbark Hickory", "American Hophornbeam", "American Hornbeam", "American Larch", "Black Locust",
            "Honey-Locust", "The Maples", "Red Maple", "Silver Maple", "Sugar Maple", "Black Oak", "Chestnut Oak",
            "Northern Red Oak", "Scarlet Oak", "White Oak", "Eastern White Pine", "Pitch Pine", "Red Pine",
            "Eastern Redcedar", "Sassafras", "Shadbush", "Red Spruce", "White Spruce", "Sycamore", "Tulip Tree",
            "Black Walnut", "Black Willow"};
}



