package pl.codementors.ent.model;

import java.io.Serializable;

/**
 * Ent class.
 *
 * @author Paweł Wieraszka
 */
public class Ent implements Serializable {

    /**
     * Enum with two types of ent.
     */
    public enum Type {
        CONIFER,
        LEAFY;
    }

    /**
     * The year of planting the seedling.
     */
    private int yearOfPlanting;

    /**
     * The height of ent.
     */
    private int height;

    /**
     * The species of ent.
     */
    private String species;

    /**
     * The type of ent.
     */
    private Type type;

    public Ent() {
    }

    public Ent(int yearOfPlanting, int height, String species, Type type) {
        this.yearOfPlanting = yearOfPlanting;
        this.height = height;
        this.species = species;
        this.type = type;
    }

    public int getYearOfPlanting() {
        return yearOfPlanting;
    }

    public void setYearOfPlanting(int yearOfPlanting) {
        this.yearOfPlanting = yearOfPlanting;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
